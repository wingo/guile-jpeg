;;; guix potluck package
;;; Copyright (C) 2017 Andy Wingo

;;; This file is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.  No warranty.  See
;;; https://www.gnu.org/licenses/gpl.html for a copy of the GPLv3.

(potluck-package
  (name "guile-jpeg")
  (version "v1.0.0")
  (source
    (potluck-source
      (git-uri "https://gitlab.com/wingo/guile-jpeg")
      (git-commit "ec5cc2fa57b583468edfacc3bb7dbf007a91ff5f")
      (sha256 "1a43bivx8wqvbic5r2rqd2ix9fir36q2z2iy88bfa6s4rvgw5c4a")))
  (build-system 'gnu)
  (inputs '("guile"))
  (native-inputs '("autoconf" "automake" "libtool" "pkg-config"))
  (propagated-inputs '())
  (arguments
    '(#:phases
      (modify-phases
        %standard-phases
        (add-before
          'configure
          'autoconf
          (lambda _ (zero? (system* "autoreconf" "-vfi")))))))
  (home-page "https://gitlab.com/wingo/guile-jpeg")
  (synopsis "JPEG manipulation library in Guile Scheme")
  (description "Guile-JPEG is a Guile module that allows developers to
parse, manipulate, reassemble, decode, and encode JPEG images.  It
also includes some facilities for raw image colorspace conversion and
scaling.")
  (license 'gpl3+))
